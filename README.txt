How to compile/run the Central Router configurator QT GUI
---------------------------------------------------------
## Qt version to be installed
Any Qt4 or Qt5 version should be okay,
but let me know if you run into problems (boterenbrood@nikhef.nl).

## Environmental variables to be set:
That depends on your Qt installation;
for example, I added two lines to my .bashrc file, similar to these
(one for the Qt tools, one for the Qt libs, resp.):

export PATH=/data/atlas/tdaq/qt551/5.5/gcc_64/bin:${PATH}
export LD_LIBRARY_PATH=/data/atlas/tdaq/qt551/5.5/gcc_64/lib:${LD_LIBRARY_PATH}

-- old export PATH=/localstore/Qt/5.4/gcc_64/bin:${PATH}
-- old export LD_LIBRARY_PATH=/localstore/Qt/5.4/gcc_64/lib:${LD_LIBRARY_PATH}

## Build the ElinkConfig tool:
$ cd /path_to_folder/software/elinkconfig
$ mkdir build
$ cd build
$ cmake ..
$ make
(OR
 $ cd /path_to_folder/software/elinkconfig
 $ qmake
 $ make
 NB: for the time being the tool depends on Jos' FelixCard class by including
     the associated lib in the linking process (so it needs to exist!):
     ../fel/build/libfel-lib.a
 The resulting (release-version) executable is created in folder 'release'.
 'qmake' generates the Makefile(s) and in principle has to be run only once. 
 To create a debug-version:
 $ make debug
 (the resulting executable is in folder 'debug').)

## Running the elinkconfig tool
$ cd /path_to_folder/software/elinkconfig/build
$ ./elinkconfig &
The panel with the main dialog that pops up displays an interface
to allow the user to configure a single 'E-group' in the 'from-GBT' direction
as well as one in the 'to-GBT' direction
(directions also known as 'to-Host' and 'from-Host' resp.).

By default at startup of the tool, the E-group configuration of all E-groups
for all GBT links is set to eight 2-bit wide E-links in 8b10b mode.

One can either read an existing configuration from an FLX-card
currently installed in the system by selecting the card to read from using
the 'From FLX' combobox followed by clicking the 'Read...' button,
or a previously made configuration saved in the tool's format
(using the 'Save..." button) can be loaded into the tool by using
the 'Open...' button.

The GBT link number spinbox allows one to select the GBT link number to display
and to configure; the E-group buttons allows one to select the particular E-group
to display and to (re)configure
(7 or 5 in the 'from-GBT' direction and 5 or 3 in the 'to-GBT' direction,
depending on the selection of 'wide mode' or 'normal mode' for the GBT link).

The 'Replicate...' buttons can be used to copy an E-group configuration
to one or more other E-groups, or to copy a GBT configuration (all its E-groups)
to one or more other GBTs (of up to 24 possible GBT links).

The 'Disable' buttons can be used to disable all E-links of the shown E-group at once.
