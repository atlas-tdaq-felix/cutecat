#include <stdio.h>
#include <string.h>
#include <sys/file.h>
#include <iostream>

#include <QMessageBox>
#include <QString>
#include <QFile>
#include <QTextStream>
#include <QHeaderView>

#include "flxcard/FlxCard.h"
#include "flxcard/FlxException.h"
#include "thecat.h"
#include "ui_thecat.h"


thecat::thecat(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::thecat)
{
  ui->setupUi(this);
}

thecat::~thecat()
{
  delete ui;
}

void thecat::on_pushButton_clicked()
{
    FILE *fd;
    int cards_found = 0, wrong_cards = 0, card_errors = 0, num_cards = 0;
    char textline[200];

    ui->inventory_window->clear();
    ui->debug_window->append("Evaluating /proc/flx");

    // Get the card inventory
    if ((fd = fopen("/proc/flx", "r") ) == 0)
    {
      QMessageBox::critical(this, "CuteCat error", "Failed to open /proc/flx");
      return;
    }

    ui->debug_window->append("/proc/flx opened");

    while(1)
    {
      fgets(textline,200,fd);
      textline[strlen(textline) - 1] = 32;  //remove the linefeed because QString.append will add one anyway.

      ui->debug_window->append(textline);
      if (strstr(textline, "Card type") != 0)
      {
        ui->inventory_window->append(textline);
        cards_found++;
        if (strstr(textline, "712") == 0)
        {
          wrong_cards = 1;
        }
      }
      if (strstr(textline, "command") != 0)
        break;

      if (strstr(textline, "Error") != 0)
      {
        card_errors++;
        ui->inventory_window->append(textline);
      }
    }
    if (cards_found == 0)
    {
      QMessageBox::critical(this, "CuteCat error", "No FLX cards found");
      return;
    }

    fclose(fd);

    num_cards = FlxCard::number_of_cards();
    ui->inventory_window->append("Total number of cards = " + QString::number(num_cards));

    if(wrong_cards)
      QMessageBox::warning(this, "CuteCat warning", "This tool only support FLX-712 cards. You better quit.");

    if(cards_found != num_cards)
      QMessageBox::warning(this, "CuteCat warning", "Some cards seem to lack F/W. You better quit this program and analyze /proc/flx.");

    if(card_errors)
      QMessageBox::warning(this, "CuteCat warning", "/proc/flx reports errors. Quitt this application and fix them");

}


void thecat::on_pod_button_clicked()
{
  monitoring_data_t moda, moda2;
  FlxCard flxCard;
  u_int numchan, maxpod, podnum;

  ui->debug_window->clear();

  try
  {
    ui->pod_message->setText("Please wait.....");
    QCoreApplication::processEvents();

    flxCard.card_open(0, 0);  //MJMJ Make the device number (first 0) a free parameter

    moda = flxCard.get_monitoring_data(POD_MONITORING);   //get the latched values
    moda2 = flxCard.get_monitoring_data(POD_MONITORING);  //read again to also get the current values
    numchan = flxCard.number_of_channels() * 2;           //MJ: This is not correct if we respect the -d
    numchan = numchan * 2;                                //This multiplication by two is because one channel has two ends (RX and TX)

    ui->pod_message->setText("formatting data.....");
    ui->debug_window->append("MiniPODs");
    ui->debug_window->append("========");

    // maxpod = moda.n_pods;
    maxpod = 8;

    if (ui->show_all->isChecked())
      ui->debug_window->append("All " + QString::number(maxpod) + " MiniPODs will be shown");
    else
    {
      if ((numchan / 12) < maxpod)
        maxpod = numchan / 12;

      ui->debug_window->append("Only the " + QString::number(maxpod) + " active MiniPODs will be shown");
    }

    ui->pod_table->setRowCount(3);
    ui->pod_table->setColumnCount(maxpod);

    for(podnum = 0; podnum < maxpod; podnum++)
      ui->pod_table->setHorizontalHeaderItem(podnum, new QTableWidgetItem(moda.minipod[podnum].name));

    ui->pod_table->setVerticalHeaderItem(0, new QTableWidgetItem("Temperature [C]"));
    ui->pod_table->setVerticalHeaderItem(1, new QTableWidgetItem("3.3 VCC [V]"));
    ui->pod_table->setVerticalHeaderItem(2, new QTableWidgetItem("2.5 VCC [V]"));

    for(podnum = 0; podnum < maxpod; podnum++)
    {
      QTableWidgetItem *pod_temp = new QTableWidgetItem(QString::number(moda.minipod[podnum].temp));
      ui->pod_table->setItem(0, podnum, pod_temp);
    }

    for(podnum = 0; podnum < maxpod; podnum++)
    {
      QTableWidgetItem *pod_v33 = new QTableWidgetItem(QString::number(moda.minipod[podnum].v33));
      ui->pod_table->setItem(1, podnum, pod_v33);
    }

    for(podnum = 0; podnum < maxpod; podnum++)
    {
      QTableWidgetItem *pod_v25 = new QTableWidgetItem(QString::number(moda.minipod[podnum].v25));
      ui->pod_table->setItem(2, podnum, pod_v25);
    }

    ui->pod_table->resizeRowsToContents();
    ui->pod_table->resizeColumnsToContents();
    ui->pod_table->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->pod_table->verticalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    ui->pod_table2->setRowCount(maxpod);
    ui->pod_table2->setColumnCount(12);

    for(int colnum = 0; colnum < 12; colnum++)
      ui->pod_table2->setHorizontalHeaderItem(colnum, new QTableWidgetItem(QString::number(colnum)));

    for(podnum = 0; podnum < maxpod; podnum++)
    {
      ui->pod_table2->setVerticalHeaderItem(podnum, new QTableWidgetItem(moda.minipod[podnum].name));

      ui->pod_table2->setItem(podnum, 0, new QTableWidgetItem(QString((moda2.minipod[podnum].los & 0x1)?"-":"#") + QString((moda.minipod[podnum].los & 0x1)?"(-)":"(#)")));
      ui->pod_table2->setItem(podnum, 1, new QTableWidgetItem(QString((moda2.minipod[podnum].los & 0x2)?"-":"#") + QString((moda.minipod[podnum].los & 0x2)?"(-)":"(#)")));
      ui->pod_table2->setItem(podnum, 2, new QTableWidgetItem(QString((moda2.minipod[podnum].los & 0x4)?"-":"#") + QString((moda.minipod[podnum].los & 0x4)?"(-)":"(#)")));
      ui->pod_table2->setItem(podnum, 3, new QTableWidgetItem(QString((moda2.minipod[podnum].los & 0x8)?"-":"#") + QString((moda.minipod[podnum].los & 0x8)?"(-)":"(#)")));
      ui->pod_table2->setItem(podnum, 4, new QTableWidgetItem(QString((moda2.minipod[podnum].los & 0x10)?"-":"#") + QString((moda.minipod[podnum].los & 0x10)?"(-)":"(#)")));
      ui->pod_table2->setItem(podnum, 5, new QTableWidgetItem(QString((moda2.minipod[podnum].los & 0x20)?"-":"#") + QString((moda.minipod[podnum].los & 0x20)?"(-)":"(#)")));
      ui->pod_table2->setItem(podnum, 6, new QTableWidgetItem(QString((moda2.minipod[podnum].los & 0x40)?"-":"#") + QString((moda.minipod[podnum].los & 0x40)?"(-)":"(#)")));
      ui->pod_table2->setItem(podnum, 7, new QTableWidgetItem(QString((moda2.minipod[podnum].los & 0x80)?"-":"#") + QString((moda.minipod[podnum].los & 0x80)?"(-)":"(#)")));
      ui->pod_table2->setItem(podnum, 8, new QTableWidgetItem(QString((moda2.minipod[podnum].los & 0x100)?"-":"#") + QString((moda.minipod[podnum].los & 0x100)?"(-)":"(#)")));
      ui->pod_table2->setItem(podnum, 9, new QTableWidgetItem(QString((moda2.minipod[podnum].los & 0x200)?"-":"#") + QString((moda.minipod[podnum].los & 0x200)?"(-)":"(#)")));
      ui->pod_table2->setItem(podnum, 10, new QTableWidgetItem(QString((moda2.minipod[podnum].los & 0x400)?"-":"#") + QString((moda.minipod[podnum].los & 0x400)?"(-)":"(#)")));
      ui->pod_table2->setItem(podnum, 11, new QTableWidgetItem(QString((moda2.minipod[podnum].los & 0x800)?"-":"#") + QString((moda.minipod[podnum].los & 0x800)?"(-)":"(#)")));
    }

    ui->pod_table2->resizeRowsToContents();
    ui->pod_table2->resizeColumnsToContents();
    ui->pod_table2->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->pod_table2->verticalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    ui->pod_table3->setRowCount(maxpod);
    ui->pod_table3->setColumnCount(12);

    for(int colnum = 0; colnum < 12; colnum++)
      ui->pod_table3->setHorizontalHeaderItem(colnum, new QTableWidgetItem(QString::number(colnum)));

    for(podnum = 0; podnum < maxpod; podnum++)
    {
      ui->pod_table3->setVerticalHeaderItem(podnum, new QTableWidgetItem(moda.minipod[podnum].name));
      ui->pod_table3->setItem(podnum, 0, new QTableWidgetItem(QString::number(moda.minipod[podnum].optical_power[0])));
      ui->pod_table3->setItem(podnum, 1, new QTableWidgetItem(QString::number(moda.minipod[podnum].optical_power[1])));
      ui->pod_table3->setItem(podnum, 2, new QTableWidgetItem(QString::number(moda.minipod[podnum].optical_power[2])));
      ui->pod_table3->setItem(podnum, 3, new QTableWidgetItem(QString::number(moda.minipod[podnum].optical_power[3])));
      ui->pod_table3->setItem(podnum, 4, new QTableWidgetItem(QString::number(moda.minipod[podnum].optical_power[4])));
      ui->pod_table3->setItem(podnum, 5, new QTableWidgetItem(QString::number(moda.minipod[podnum].optical_power[5])));
      ui->pod_table3->setItem(podnum, 6, new QTableWidgetItem(QString::number(moda.minipod[podnum].optical_power[6])));
      ui->pod_table3->setItem(podnum, 7, new QTableWidgetItem(QString::number(moda.minipod[podnum].optical_power[7])));
      ui->pod_table3->setItem(podnum, 8, new QTableWidgetItem(QString::number(moda.minipod[podnum].optical_power[8])));
      ui->pod_table3->setItem(podnum, 9, new QTableWidgetItem(QString::number(moda.minipod[podnum].optical_power[9])));
      ui->pod_table3->setItem(podnum, 10, new QTableWidgetItem(QString::number(moda.minipod[podnum].optical_power[10])));
      ui->pod_table3->setItem(podnum, 11, new QTableWidgetItem(QString::number(moda.minipod[podnum].optical_power[11])));
    }

    ui->pod_table3->resizeRowsToContents();
    ui->pod_table3->resizeColumnsToContents();
    ui->pod_table3->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->pod_table3->verticalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    ui->pod_table4->setRowCount(6);
    ui->pod_table4->setColumnCount(maxpod);

    for(podnum = 0; podnum < maxpod; podnum++)
      ui->pod_table4->setHorizontalHeaderItem(podnum, new QTableWidgetItem(moda.minipod[podnum].name));

    ui->pod_table4->setVerticalHeaderItem(0, new QTableWidgetItem("Name"));
    ui->pod_table4->setVerticalHeaderItem(1, new QTableWidgetItem("OUI"));
    ui->pod_table4->setVerticalHeaderItem(2, new QTableWidgetItem("Part number"));
    ui->pod_table4->setVerticalHeaderItem(3, new QTableWidgetItem("Revision number"));
    ui->pod_table4->setVerticalHeaderItem(4, new QTableWidgetItem("Serial number"));
    ui->pod_table4->setVerticalHeaderItem(5, new QTableWidgetItem("Date code"));

    for(podnum = 0; podnum < maxpod; podnum++)
    {
      ui->pod_table4->setItem(0, podnum, new QTableWidgetItem(moda.minipod[podnum].vname));
      ui->pod_table4->setItem(2, podnum, new QTableWidgetItem(QString::number(moda.minipod[podnum].voui[0]) + QString::number(moda.minipod[podnum].voui[1]) + QString::number(moda.minipod[podnum].voui[2])));
      ui->pod_table4->setItem(1, podnum, new QTableWidgetItem(moda.minipod[podnum].vpnum));
      ui->pod_table4->setItem(3, podnum, new QTableWidgetItem(QString::number(moda.minipod[podnum].vrev[0]) + QString::number(moda.minipod[podnum].vrev[1])));
      ui->pod_table4->setItem(4, podnum, new QTableWidgetItem(moda.minipod[podnum].vsernum));
      ui->pod_table4->setItem(5, podnum, new QTableWidgetItem(moda.minipod[podnum].vdate));
    }
    ui->pod_table4->resizeRowsToContents();
    ui->pod_table4->resizeColumnsToContents();
    ui->pod_table4->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->pod_table4->verticalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    flxCard.card_close();
   }
   catch(FlxException &ex)
   {
     std::cout << "ERROR. Exception thrown: " << ex.what() << std:: endl;
     QMessageBox::critical(this, "CuteCat error", "Exception from FLxCard");
     exit(-1);
   }
  ui->pod_message->setText("Done!");
}


void thecat::on_test_button_clicked()
{
  int podnum;

  ui->test_table->setRowCount(3);
  ui->test_table->setColumnCount(9);

  for(podnum = 0; podnum < 9; podnum++)
  {
    QTableWidgetItem *podname = new QTableWidgetItem("1234");
    ui->test_table->setHorizontalHeaderItem(podnum, podname);
  }
}

void thecat::on_fpga_button_clicked()
{
  FlxCard flxCard;

  try
  {
    flxCard.card_open(0, 0);  //MJMJ Make the device number (first 0) a free parameter
    monitoring_data_t moda = flxCard.get_monitoring_data(FPGA_MONITORING);

    ui->fpga_table->setRowCount(5);
    ui->fpga_table->setColumnCount(1);
    ui->fpga_table->setHorizontalHeaderItem(0, new QTableWidgetItem("Parameter"));
    ui->fpga_table->setHorizontalHeaderItem(1, new QTableWidgetItem("Value"));
    ui->fpga_table->setVerticalHeaderItem(0, new QTableWidgetItem("Temperature"));
    ui->fpga_table->setVerticalHeaderItem(1, new QTableWidgetItem("Internal voltage  [VccInt]"));
    ui->fpga_table->setVerticalHeaderItem(2, new QTableWidgetItem("Auxiliary voltage [VccAux]"));
    ui->fpga_table->setVerticalHeaderItem(3, new QTableWidgetItem("BRAM voltage      [VccBRAM]"));
    ui->fpga_table->setVerticalHeaderItem(4, new QTableWidgetItem("DNA"));
    ui->fpga_table->setItem(0, 0, new QTableWidgetItem(QString::number(moda.fpga.temperature)));
    ui->fpga_table->setItem(1, 0, new QTableWidgetItem(QString::number(moda.fpga.vccint)));
    ui->fpga_table->setItem(2, 0, new QTableWidgetItem(QString::number(moda.fpga.vccaux)));
    ui->fpga_table->setItem(3, 0, new QTableWidgetItem(QString::number(moda.fpga.vccbram)));
    ui->fpga_table->setItem(4, 0, new QTableWidgetItem(QString::number(moda.fpga.dna)));

    ui->fpga_table->resizeRowsToContents();
    ui->fpga_table->resizeColumnsToContents();
    ui->fpga_table->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->fpga_table->verticalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    flxCard.card_close();
    }
    catch(FlxException &ex)
    {
      std::cout << "ERROR. Exception thrown: " << ex.what() << std:: endl;
      QMessageBox::critical(this, "CuteCat error", "Exception from FLxCard");
      exit(-1);
    }
}

void thecat::on_ltc_button_clicked()
{
  FlxCard flxCard;

  try
  {
    flxCard.card_open(0, 0);  //MJMJ Make the device number (first 0) a free parameter
    monitoring_data_t moda = flxCard.get_monitoring_data(LTC_MONITORING);

    ui->ltc_table_1->setRowCount(9);
    ui->ltc_table_1->setColumnCount(2);
    ui->ltc_table_1->setHorizontalHeaderItem(0, new QTableWidgetItem("Value"));
    ui->ltc_table_1->setHorizontalHeaderItem(1, new QTableWidgetItem("Unit"));

    ui->ltc_table_1->setVerticalHeaderItem(0, new QTableWidgetItem("VCCINT current"));
    ui->ltc_table_1->setVerticalHeaderItem(1, new QTableWidgetItem("VCCINT voltage"));
    ui->ltc_table_1->setVerticalHeaderItem(2, new QTableWidgetItem("MGTAVCC current"));
    ui->ltc_table_1->setVerticalHeaderItem(3, new QTableWidgetItem("MGTAVCC voltage"));
    ui->ltc_table_1->setVerticalHeaderItem(4, new QTableWidgetItem("FPGA internal diode temperature"));
    ui->ltc_table_1->setVerticalHeaderItem(5, new QTableWidgetItem("MGTAVTT voltage"));
    ui->ltc_table_1->setVerticalHeaderItem(6, new QTableWidgetItem("MGTAVTT current"));
    ui->ltc_table_1->setVerticalHeaderItem(7, new QTableWidgetItem("LTC2991_1 internal temperature"));
    ui->ltc_table_1->setVerticalHeaderItem(8, new QTableWidgetItem("VCC"));

    ui->ltc_table_1->setItem(0, 0, new QTableWidgetItem(QString::number(moda.ltc1.VCCINT_current)));
    ui->ltc_table_1->setItem(1, 0, new QTableWidgetItem(QString::number(moda.ltc1.VCCINT_voltage)));
    ui->ltc_table_1->setItem(2, 0, new QTableWidgetItem(QString::number(moda.ltc1.MGTAVCC_current)));
    ui->ltc_table_1->setItem(3, 0, new QTableWidgetItem(QString::number(moda.ltc1.MGTAVCC_voltage)));
    ui->ltc_table_1->setItem(4, 0, new QTableWidgetItem(QString::number(moda.ltc1.FPGA_internal_diode_temperature)));
    ui->ltc_table_1->setItem(5, 0, new QTableWidgetItem(QString::number(moda.ltc1.MGTAVTT_current)));
    ui->ltc_table_1->setItem(6, 0, new QTableWidgetItem(QString::number(moda.ltc1.MGTAVTT_voltage)));
    ui->ltc_table_1->setItem(7, 0, new QTableWidgetItem(QString::number(moda.ltc1.LTC2991_1_internal_temperature)));
    ui->ltc_table_1->setItem(8, 0, new QTableWidgetItem(QString::number(moda.ltc1.vcc)));

    ui->ltc_table_1->setItem(0, 1, new QTableWidgetItem("A"));
    ui->ltc_table_1->setItem(1, 1, new QTableWidgetItem("V"));
    ui->ltc_table_1->setItem(2, 1, new QTableWidgetItem("A"));
    ui->ltc_table_1->setItem(3, 1, new QTableWidgetItem("V"));
    ui->ltc_table_1->setItem(4, 1, new QTableWidgetItem("C"));
    ui->ltc_table_1->setItem(5, 1, new QTableWidgetItem("A"));
    ui->ltc_table_1->setItem(6, 1, new QTableWidgetItem("V"));
    ui->ltc_table_1->setItem(7, 1, new QTableWidgetItem("C"));
    ui->ltc_table_1->setItem(8, 1, new QTableWidgetItem("V"));

    ui->ltc_table_1->resizeRowsToContents();
    ui->ltc_table_1->resizeColumnsToContents();
    ui->ltc_table_1->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->ltc_table_1->verticalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    ui->ltc_table_2->setRowCount(8);
    ui->ltc_table_2->setColumnCount(2);
    ui->ltc_table_2->setHorizontalHeaderItem(0, new QTableWidgetItem("Value"));
    ui->ltc_table_2->setHorizontalHeaderItem(1, new QTableWidgetItem("Unit"));

    ui->ltc_table_2->setVerticalHeaderItem(0, new QTableWidgetItem("PEX0P9V current"));
    ui->ltc_table_2->setVerticalHeaderItem(1, new QTableWidgetItem("PEX0P9V voltage"));
    ui->ltc_table_2->setVerticalHeaderItem(2, new QTableWidgetItem("SYS18 current"));
    ui->ltc_table_2->setVerticalHeaderItem(3, new QTableWidgetItem("SYS18 voltage"));
    ui->ltc_table_2->setVerticalHeaderItem(4, new QTableWidgetItem("SYS25 current"));
    ui->ltc_table_2->setVerticalHeaderItem(5, new QTableWidgetItem("SYS25 voltage"));
    ui->ltc_table_2->setVerticalHeaderItem(6, new QTableWidgetItem("EX8732 internal diode temperature"));
    ui->ltc_table_2->setVerticalHeaderItem(7, new QTableWidgetItem("LTC2991_2 internal temperature"));
    ui->ltc_table_2->setVerticalHeaderItem(8, new QTableWidgetItem("VCC"));

    ui->ltc_table_2->setItem(0, 0, new QTableWidgetItem(QString::number(moda.ltc2.PEX0P9V_current)));
    ui->ltc_table_2->setItem(1, 0, new QTableWidgetItem(QString::number(moda.ltc2.PEX0P9V_voltage)));
    ui->ltc_table_2->setItem(2, 0, new QTableWidgetItem(QString::number(moda.ltc2.SYS18_current)));
    ui->ltc_table_2->setItem(3, 0, new QTableWidgetItem(QString::number(moda.ltc2.SYS18_voltage)));
    ui->ltc_table_2->setItem(4, 0, new QTableWidgetItem(QString::number(moda.ltc2.SYS25_current)));
    ui->ltc_table_2->setItem(5, 0, new QTableWidgetItem(QString::number(moda.ltc2.SYS25_voltage)));
    ui->ltc_table_2->setItem(6, 0, new QTableWidgetItem(QString::number(moda.ltc2.PEX8732_internal_diode_temperature)));
    ui->ltc_table_2->setItem(7, 0, new QTableWidgetItem(QString::number(moda.ltc2.LTC2991_2_internal_temperature)));
    ui->ltc_table_2->setItem(8, 0, new QTableWidgetItem(QString::number(moda.ltc2.vcc)));

    ui->ltc_table_2->setItem(0, 1, new QTableWidgetItem("A"));
    ui->ltc_table_2->setItem(1, 1, new QTableWidgetItem("V"));
    ui->ltc_table_2->setItem(2, 1, new QTableWidgetItem("A"));
    ui->ltc_table_2->setItem(3, 1, new QTableWidgetItem("V"));
    ui->ltc_table_2->setItem(4, 1, new QTableWidgetItem("A"));
    ui->ltc_table_2->setItem(5, 1, new QTableWidgetItem("V"));
    ui->ltc_table_2->setItem(6, 1, new QTableWidgetItem("C"));
    ui->ltc_table_2->setItem(7, 1, new QTableWidgetItem("C"));
    ui->ltc_table_2->setItem(8, 1, new QTableWidgetItem("V"));

    ui->ltc_table_2->resizeRowsToContents();
    ui->ltc_table_2->resizeColumnsToContents();
    ui->ltc_table_2->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->ltc_table_2->verticalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    flxCard.card_close();
    }
    catch(FlxException &ex)
    {
      std::cout << "ERROR. Exception thrown: " << ex.what() << std:: endl;
      QMessageBox::critical(this, "CuteCat error", "Exception from FLxCard");
      exit(-1);
    }
}



void thecat::on_ltc_save_clicked()
{
  int table_row, table_column;
  QString textData;
  QFile txtFile("ltcdata.txt");

  for(table_column = 0; table_column < ui->ltc_table_1->columnCount(); table_column++)
  {
    ui->debug_window->append(ui->ltc_table_1->horizontalHeaderItem(table_column)->data(Qt::DisplayRole).toString());
    textData += ui->ltc_table_1->horizontalHeaderItem(table_column)->data(Qt::DisplayRole).toString();
    textData += " | ";
  }

  textData += "\n";

  for(table_row = 0; table_row < ui->ltc_table_1->rowCount(); table_row++)
  {
    textData += ui->ltc_table_1->verticalHeaderItem(table_row)->data(Qt::DisplayRole).toString();
    for(table_column = 0; table_column < ui->ltc_table_1->columnCount(); table_column++)
    {
      textData += " | ";
      textData += ui->ltc_table_1->item(table_row, table_column)->text();
    }
    textData += "\n";
  }

  if (!txtFile.open(QIODevice::WriteOnly))
  {
    QMessageBox::critical(this, "Can't open output file", "Linux error");
    return;

  }

  QTextStream out(&txtFile);
  out << textData;
  txtFile.close();
}


void thecat::on_gbt_button_clicked()
{
  u_long channels = 0, number_channels = 0;
  FlxCard flxCard;

  try
  {
    flxCard.card_open(0, 0);  //MJMJ Make the device number (first 0) a free parameter

    channels = flxCard.cfg_get_reg(REG_GBT_ALIGNMENT_DONE);
    number_channels = 2 * flxCard.cfg_get_option(BF_NUM_OF_CHANNELS);      //Multiply by 2 to get total number of channels on the card.

    ui->gbt_table->setRowCount(1);
    ui->gbt_table->setColumnCount(number_channels);

    ui->gbt_table->setVerticalHeaderItem(0, new QTableWidgetItem("Channel aligned"));
    for(u_int gbt_chan = 0; gbt_chan < number_channels; gbt_chan++)
    {
        ui->gbt_table->setHorizontalHeaderItem(gbt_chan, new QTableWidgetItem(QString::number(gbt_chan)));

        if ((channels & (1ul << gbt_chan)) > 1)
        {
          QTableWidgetItem *celltext = new QTableWidgetItem("YES");
          celltext->setTextColor(Qt::green);
          ui->gbt_table->setItem(0, gbt_chan, celltext);
        }
        else
        {
          QTableWidgetItem *celltext = new QTableWidgetItem("NO");
          celltext->setTextColor(Qt::red);
          ui->gbt_table->setItem(0, gbt_chan, celltext);
        }
    }

    ui->gbt_table->resizeRowsToContents();
    ui->gbt_table->resizeColumnsToContents();
    ui->gbt_table->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->gbt_table->verticalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    flxCard.card_close();
  }
  catch(FlxException &ex)
  {
    std::cout << "ERROR. Exception thrown: " << ex.what() << std:: endl;
    QMessageBox::critical(this, "CuteCat error", "Exception from FLxCard");
    exit(-1);
  }
}
