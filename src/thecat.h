#ifndef THECAT_H
#define THECAT_H

#include <QMainWindow>

namespace Ui {
class thecat;
}

class thecat : public QMainWindow
{
  Q_OBJECT

public:
  explicit thecat(QWidget *parent = 0);
  ~thecat();

private slots:
  void on_pushButton_clicked();

  void on_pod_button_clicked();

  void on_test_button_clicked();

  void on_fpga_button_clicked();

  void on_ltc_button_clicked();

  void on_gbt_button_clicked();

  void on_ltc_save_clicked();

private:
  Ui::thecat *ui;
};

#endif // THECAT_H
