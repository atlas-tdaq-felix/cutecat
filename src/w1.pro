#-------------------------------------------------
#
# Project created by QtCreator 2019-01-25T13:49:23
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = w1
TEMPLATE = app


SOURCES += main.cpp\
        thecat.cpp

HEADERS  += thecat.h

FORMS    += thecat.ui

INCLUDEPATH += /afs/cern.ch/user/j/joos/DAQ/FELIX/software/flxcard
INCLUDEPATH += /afs/cern.ch/user/j/joos/DAQ/FELIX/software/regmap
INCLUDEPATH += /afs/cern.ch/atlas/project/tdaq/inst/tdaq/tdaq-07-01-00/installed/include

